const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    totalAmount:{
        type: Number,
        required: [true, `Total Amount is required`]
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    userId:{
        type: String,
        required: [true, `User ID is required`]
    },
    transactions:[{
        productId: {
            type: String,
            required: [true, `Product ID is required`]
        },
        quantity: {
            type: Number,
            default: 1
        }
    }],
    isActive: {
        type: Boolean,
        default: true
    }

}, {timestamps: true})

module.exports = mongoose.model("Order", orderSchema);