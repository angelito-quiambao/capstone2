const express = require('express');
const router = express.Router();

const{
    register,
    checkEmail,
    login,
    getAllUsers,
    profile,
    updatePassword,
    setAdmin,
    setUser
} = require('./../controllers/userControllers')

//User authentication
const {
    verify,
    decode,
    verifyAdmin
} = require('./../auth');

//User registration
router.post('/register', async (req, res) => {
    //console.log("test @routes")
    try{
        await register(req.body).then(result => res.send (result))
    }
    catch(err){
        res.status(500).json(err)
    }
})

//Check if email address already exist
router.post('/email-exists', async (req, res) =>{
    try{
       await checkEmail(req.body).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)
    }
})

//User login (for token session creation)
router.post('/login', async (req, res) => {
    // console.log("test @routes")
   
    try{
       await login(req.body).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)
    } 
})

//Get all users isAdmin
router.get('/', verifyAdmin, async (req, res) =>{
    // console.log("test @routes")
    
        try{
           await getAllUsers().then(result => res.send (result))
        }
        catch(err){
            res.status.apply(500).json(err)
        }
    })

//Get User profile
router.get('/profile', verify, async (req, res) =>{

    const userId = decode(req.headers.authorization).id

    try{
        // console.log("test @routes")
        await profile(userId).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)
    }
})

//update user's password
router.patch('/update-password', verify, async (req,res) =>{
// console.log("test @routes")

    const userId = decode(req.headers.authorization).id
    // console.log(userId)

    try {
        await updatePassword(userId, req.body).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)
    }
})

//Set user as admin (admin only)
router.patch('/:userId/setAsAdmin', verifyAdmin, async (req, res) => {
    // console.log("test @routes")
    const userId = req.params.userId
    // console.log(userId)
    try{
        await setAdmin(userId).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)
    }
})

//Set admin as user (admin only)
router.patch('/:userId/setAsUser', verifyAdmin, async (req, res) => {
    // console.log("test @routes")
    const userId = req.params.userId
    // console.log(userId)
    try{
        await setUser(userId).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)
    }
})

//Export the router module to be used in index.js file
module.exports = router;