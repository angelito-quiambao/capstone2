//import Schema
const Product = require ('../models/Product');
const Order = require ('../models/Order');

//Non-admin User checkout (Create Order)
module.exports.checkOut = async(data) =>{
    // console.log("test @controllers")

    const {userId, productId, quantity} = data


    // get product information specifically price
    const getProduct = await Product.findByIdAndUpdate(productId, {$inc: {quantity: -quantity}}, {new: true}).then((result, err) => {
        if(result){
            return result
        }
        else{
            return err
        }
    })

    // console.log(getProduct)

    const newOrder = Order({
        totalAmount: (getProduct.price * quantity),
        userId: userId,
        transactions:[{
            productId: productId,
            quantity: quantity
        }]
    })

    console.log(newOrder)

    return await newOrder.save().then(result => {
        if(result){
            return true
        }
        else{
            if (result == null){
                return false
            }
        }
    })

}

//Add products if order already exists and active
module.exports.addOrders = async(data) =>{

    const {userId, productId, quantity} = data

    const getProduct = await Product.findByIdAndUpdate(productId, {$inc: {quantity: -quantity}}, {new: true}).then((result, err) => {
        if(result){
            console.log(result)
            return result
        }
        else{
            return err
        }
    })

    return await Order.findOne({$and:[{userId: userId}, {isActive:true}]}).then(result => {
        
       const index = result.transactions.findIndex(data =>{
           return data.productId == productId
       })

    //    console.log(index)
       if(index == -1 ){
            result.totalAmount += (getProduct.price * quantity)
            result.transactions.push({productId: productId, quantity: quantity})
            
            return result.save().then(order => {
                if(order){
                    console.log(result)
                    return true
                }
                else{
                    return false
                }
            })

        }
        else{
            // return {message: "Product already exists in your order!"}
            result.totalAmount += (getProduct.price * quantity)
            result.transactions[index].quantity += parseInt(quantity)

            return result.save().then(order => {
                if(order){
                    console.log(result)
                    return true
                }
                else{
                    return false
                }
            })

        }
    })

}

//remove a product on the order list
module.exports.deleteOrder = async(data) =>{
    const {userId, productId} = data

     const getProduct = await Product.findById(productId).then(result => result)

     return await Order.findOne({$and:[{userId: userId}, {isActive:true}]}).then(result => {

        const index = result.transactions.findIndex(data =>{
            return data.productId == productId
        })

        console.log(index)

        if(index != -1 ){
            quantity = result.transactions[index].quantity

            Product.findById(productId).then((result, err) => {
                if(result){
                     result.quantity += quantity
                    return result.save().then(order => {
                        if(order){
                            return true
                        }
                        else{
                            return false
                        }
                    })

                    return result
                }

                else{
                    return err
                }
            })

            result.transactions.splice(index, 1)
            result.totalAmount -= (getProduct.price * quantity)
            console.log(result)

            return result.save().then(order => {
                        if(order){
                            return true
                        }
                        else{
                            return false
                        }
                    })
        }
        else{
            return false
        }

     })
}

// Retrieve all orders (Admin only)
module.exports.getAllOrders = async () =>{
    // console.log("test @controllers")
    return await Order.find().then(result => result)
}

//Retrieve authenticated user’s orders
module.exports.userOrders = async (id) =>{
    // console.log("test @controllers")
    return await Order.findOne({$and:[{userId: id}, {isActive:true}]}).then(result => {
        if(result){
            return result
        }
        else{
            return false
        }
    })
}

//Check if Order exist
module.exports.checkOrder = async (id) =>{
    // console.log("test @controllers")
    return await Order.findOne({$and:[{userId: id}, {isActive:true}]}).then(result => {
        if(result){
            return true
        }
        else{
            return false
        }
    })
}


//Change Order status if complete
module.exports.orderStatus = async (id) =>{
    return await Order.findOneAndUpdate({$and:[{userId: id}, {isActive:true}]}, {$set:{isActive: false}}, {new: true}).then(result => result)
}