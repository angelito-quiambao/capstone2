//encryption
const CryptoJS = require("crypto-js");

//import auth.js
const { createToken } = require('./../auth');

//import Schema
const User = require ('../models/User');
const Product = require ('../models/Product');
const Order = require ('../models/Order');

//User registration
module.exports.register = async (reqBody) => {
    //console.log("test @controllers")

    const {firstName, lastName, email, password} = reqBody

    const newUser = new User({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
    })

    // console.log(newUser)

    return await newUser.save().then(result => {
        if(result){
            return true
        }
        else{
            if (result == null){
                return false
            }
        }
    })
}

//Check if email address already exist
module.exports.checkEmail = async (reqBody) =>{
    const {email} = reqBody

    return await User.findOne({email: email}).then((result, err) => {
        if(result){
            return true
        }
        else{
            if (result == null){
                return false
            }
            else{
                return err
            }
        }
    })
}

//User login (for token session creation)
module.exports.login = async (reqBody) => {
    //console.log("test @controllers")

	return await User.findOne({email: reqBody.email}).then((result, err) => {

		if(result == null){
			return false

		} else {

			if(result !== null){
				const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)
				
				if(reqBody.password == decryptedPw){
					return { token: createToken(result) }
				} else {
					return false
				}

			} else {
				return err
			}
		}
	})
}

//Get all users for setting isAdmin
module.exports.getAllUsers = async () =>{
    //console.log("test @controllers")
    
    return await User.find().then( result => result)
}

//Get User profile
module.exports.profile = async (id) =>{
    // console.log("test @controllers")
    return await User.findById(id).then(result => result)
}

//update user's password
module.exports.updatePassword = async (userId, reqBody) => {
    //console.log("test @controllers")

    let newPass = { 
        password: CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString()
    }

    let oldPass = await User.findById(userId, {_id: 0, password: 1}).then(result => {
        return CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)
    })

    // console.log(reqBody.password)
    // console.log(oldPass)
    // console.log(newPass)

    if(reqBody.password == oldPass){
        return {message: `New password is similar with Old password!`}
    }
    else{
        return await User.findByIdAndUpdate(userId, {$set: newPass}).then((result, err) => {
            if(result){
                result.password = "***"
                return result
            }
            else{
                return err
            }
        })
    }
}

//Set user as admin (admin only)
module.exports.setAdmin = async (id) =>{
    //  console.log("test @controllers")

    return await User.findByIdAndUpdate(id, {$set: {isAdmin: true}}).then((result) => {
        if(result){
            return true
        }
        else{
            return false
        }
    })
}

//Set admin as user (admin only)
module.exports.setUser = async (id) =>{
    //  console.log("test @controllers")

    return await User.findByIdAndUpdate(id, {$set: {isAdmin: false}}).then((result) => {
        if(result){
            return true
        }
        else{
            return false
        }
    })
}
